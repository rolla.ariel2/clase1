console.log("Se accede al JS");

const persona = [
    { nombre: 'Jorge', edad: 19 },
    { nombre: 'Carlos', edad: 32 },
    { nombre: 'Alberto', edad: 35 },
    { nombre: 'Dario', edad: 32 },
    { nombre: 'Horacio', edad: 15 },
    { nombre: 'Lucas', edad: 17 },
    { nombre: 'Mario', edad: 42 }

];

//Tarea Clase 2  - Punto 2
function GetStringPersonasMayores(personas){
    const personasMayoresOBJ = personas.filter(person => person.edad>25);
    const personasMayores = personasMayoresOBJ.map(function (person){
        return `${person.nombre} tiene ${person.edad} años`;
    });
    return personasMayores;
 };

 function GetStringPersonasMenores(personas){
    const personasMenoresOBJ = personas.filter(person => person.edad<=25);
    const personasMenores = personasMenoresOBJ.map(function (person){
        return `${person.nombre} tiene ${person.edad} años`;
    });
    return personasMenores;
 };



console.log(GetStringPersonasMayores(persona));

console.log(GetStringPersonasMenores(persona));


//Tarea Clase 2  - Punto 3

function inicio (){
    crearDIVS(GetStringPersonasMayores(persona));

    const miPromesa = new Promise((resolve, reject) => {
        setTimeout(() => resolve(), 2000);
    });
    
    miPromesa.then(() => {
        cargarUsuarios();
    });
    
    const boton = document.getElementById('refresh');
    boton.addEventListener('click', cargarUsuarios);
}

function crearDIVS(array){
    const main = document.getElementById("main");

     array.forEach(function (person){
        const newDiv = document.createElement("div");
        newDiv.appendChild(document.createTextNode(person));
        main.appendChild(newDiv);
    });
}





window.onload = inicio;


//Tarea Clase 3

function cargarUsuarios() {
    document.getElementById('cargando').innerHTML = 'Cargando datos de usuarios....';
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((response) => response.json())
      .then((json) => mostrarUsuarios(json));
}

function mostrarUsuarios(usuarios) {
    document.getElementById('cargando').innerHTML = '';
    const content = document.querySelector('main');
    content.innerHTML = '';
    usuarios.forEach(usuario => {
        const div = document.createElement('div');
        div.innerHTML = `
            <div class="card">
                <div><strong>${usuario.name}</strong></div>
                <div>
                    <p>${usuario.email}</p>
                    <p>${usuario.phone}</p>
                    <p><a href="${usuario.website}">${usuario.website}</a></p>
                    <p>${usuario.company.name}</p>
                </div>
            </div>
        `;
        content.appendChild(div);
    });
}




